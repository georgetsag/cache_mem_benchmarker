#!/bin/bash
echo Welcome to pintool executor by smert tsurts.
###########
#YOU MUST SET THESE PATHS IN ORDER FOR THE SCRIPT TO WORK
###########

export LD_LIBRARY_PATH=/home/george/AdvCompArch/parsec-3.0/pkgs/libs/hooks/inst/amd64-linux.gcc-serial/lib

INPUT_PATH=/home/george/AdvCompArch/parsec-3.0/parsec_workspace/inputs/ 

EXEC_PATH=/home/george/AdvCompArch/parsec-3.0/parsec_workspace/executables/

PIN_GCC_LINUX_PATH=/home/george/AdvCompArch/pin-3.13-98189-g60a6ef199-gcc-linux/pin

PINTOOL_PATH=/home/george/AdvCompArch/advcomparch-2019-2020-ex1-helpcode/pintool/obj-intel64/simulator.so

PLOT_PATH1=/home/george/AdvCompArch/plot_l1.sh

PLOT_PATH2=/home/george/AdvCompArch/plot_l2.sh

PLOT_PATH_TLB=/home/george/AdvCompArch/plot_tlb.sh

PLOT_PATH_PRF=/home/george/AdvCompArch/plot_prf.sh

#########
#########
if [ -z $LD_LIBRARY_PATH ]; then
	echo "LD_LIBRARY_PATH variable is not set"
	return 0
fi

if [ -z $INPUT_PATH ]; then
	echo "INPUT_PATH variable is not set (workspace environment inputs folder)"
	return 0
fi

if [ -z $EXEC_PATH ]; then
	echo "EXEC_PATH variable is not set"
	return 0
fi

if [ -z $PIN_GCC_LINUX_PATH ]; then
	echo "PIN_GCC_LINUX_PATH variable is not set"
	return 0
fi

if [ -z $PLOT_PATH1 ]; then
	echo "PLOT_PATH1 variable is not set"
	return 0
fi

if [ -z $PLOT_PATH2 ]; then
	echo "PLOT_PATH2 variable is not set"
	return 0
fi

if [ -z $PLOT_PATH_TLB ]; then
	echo "PLOT_PATH_TLB variable is not set"
	return 0
fi

if [ -z $PLOT_PATH_PRF ]; then
	echo "PLOT_PATH_PRF variable is not set"
	return 0
fi

declare -A inputSpecifier=(
	["blackscholes"]=" 1 ${INPUT_PATH}in_64K.txt prices.txt"
	["bodytrack"]=" ${INPUT_PATH}sequenceB_4 4 4 4000 5 0 1"
	["canneal"]=" 1 15000 2000 ${INPUT_PATH}400000.nets 128"
	["facesim"]=" -timing -threads 1"
	["ferret"]=" ${INPUT_PATH}corel lsh ${INPUT_PATH}queries 10 20 1 output.txt"
	["fluidanimate"]=" 1 5 ${INPUT_PATH}in_300K.fluid out.fluid"
	["freqmine"]=" ${INPUT_PATH}kosarak_990k.dat 790"
	["rtview"]=" ${INPUT_PATH}happy_buddha.obj -automove -nthreads 1 -frames 3 -res 1920 1080"
	["streamcluster"]=" 10 20 128 16384 16384 1000 none output.txt 1"
	["swaptions"]=" -ns 64 -sm 40000 -nt 1"
	) 

executePin() {
	echo "Executing PIN..."
	$PIN_GCC_LINUX_PATH -t $PINTOOL_PATH -o ${11} -L1c $1 -L1a $2 -L1b $3 -L2c $4 -L2a $5 -L2b $6 -TLBe $7 -TLBa $8 -TLBp $9 -L2prf ${12} -- ${EXEC_PATH}${10} ${inputSpecifier["${10}"]}
}

executeL1Procedure(){
	executePin 32 4 64 1024 8 128 64 4 4096 $1 $1_l1.out1 0
	executePin 32 8 32 1024 8 128 64 4 4096 $1 $1_l1.out2 0
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_l1.out3 0
	executePin 32 8 128 1024 8 128 64 4 4096 $1 $1_l1.out4 0
	executePin 64 4 64 1024 8 128 64 4 4096 $1 $1_l1.out5 0
	executePin 64 8 32 1024 8 128 64 4 4096 $1 $1_l1.out6 0
	executePin 64 8 64 1024 8 128 64 4 4096 $1 $1_l1.out7 0
	executePin 64 8 128 1024 8 128 64 4 4096 $1 $1_l1.out8 0
	executePin 128 8 32 1024 8 128 64 4 4096 $1 $1_l1.out9 0
	executePin 128 8 64 1024 8 128 64 4 4096 $1 $1_l1.out10 0
	executePin 128 8 128 1024 8 128 64 4 4096 $1 $1_l1.out11 0

}

plotL1Generation(){
	$PLOT_PATH1 const_cycle $1_l1.out1 $1_l1.out2 $1_l1.out3 $1_l1.out4 $1_l1.out5 $1_l1.out6 $1_l1.out7 $1_l1.out8 $1_l1.out9 $1_l1.out10 $1_l1.out11
	mv L1.png $1_L1.png
	mkdir L1_OUT
	mv $1_l1.out* L1_OUT
}

executeL2Procedure(){
	executePin 32 8 64 512 8 64 64 4 4096 $1 $1_l2.out1 0
	executePin 32 8 64 512 8 128 64 4 4096 $1 $1_l2.out2 0
	executePin 32 8 64 512 8 256 64 4 4096 $1 $1_l2.out3 0

	executePin 32 8 64 1024 8 64 64 4 4096 $1 $1_l2.out4 0
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_l2.out5 0
	executePin 32 8 64 1024 8 256 64 4 4096 $1 $1_l2.out6 0

	executePin 32 8 64 1024 16 64 64 4 4096 $1 $1_l2.out7 0
	executePin 32 8 64 1024 16 128 64 4 4096 $1 $1_l2.out8 0
	executePin 32 8 64 1024 16 256 64 4 4096 $1 $1_l2.out9 0

	executePin 32 8 64 2048 16 64 64 4 4096 $1 $1_l2.out10 0
	executePin 32 8 64 2048 16 128 64 4 4096 $1 $1_l2.out11 0
	executePin 32 8 64 2048 16 256 64 4 4096 $1 $1_l2.out12 0
}

plotL2Generation(){
	$PLOT_PATH2 const_cycle $1_l2.out1 $1_l2.out2 $1_l2.out3 $1_l2.out4 $1_l2.out5 $1_l2.out6 $1_l2.out7 $1_l2.out8 $1_l2.out9 $1_l2.out10 $1_l2.out11 $1_l2.out12
	mv L2.png $1_L2.png
	mkdir L2_OUT
	mv $1_l2.out* L2_OUT
}

executeTLBProcedure(){
	executePin 32 8 64 1024 8 128 8 4 4096 $1 $1_tlb.out1 0
	executePin 32 8 64 1024 8 128 16 4 4096 $1 $1_tlb.out2 0
	executePin 32 8 64 1024 8 128 32 4 4096 $1 $1_tlb.out3 0

	executePin 32 8 64 1024 8 128 64 1 4096 $1 $1_tlb.out4 0
	executePin 32 8 64 1024 8 128 64 2 4096 $1 $1_tlb.out5 0
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_tlb.out6 0

	executePin 32 8 64 1024 8 128 64 8 4096 $1 $1_tlb.out7 0
	executePin 32 8 64 1024 8 128 64 16 4096 $1 $1_tlb.out8 0
	executePin 32 8 64 1024 8 128 64 32 4096 $1 $1_tlb.out9 0

	executePin 32 8 64 1024 8 128 64 64 4096 $1 $1_tlb.out10 0
	executePin 32 8 64 1024 8 128 128 4 4096 $1 $1_tlb.out11 0
	executePin 32 8 64 1024 8 128 256 4 4096 $1 $1_tlb.out12 0
}

plotTLBGeneration(){
	$PLOT_PATH_TLB const_cycle $1_tlb.out1 $1_tlb.out2 $1_tlb.out3 $1_tlb.out4 $1_tlb.out5 $1_tlb.out6 $1_tlb.out7 $1_tlb.out8 $1_tlb.out9 $1_tlb.out10 $1_tlb.out11 $1_tlb.out12
	mv TLB.png $1_TLB.png
	mkdir TLB_OUT
	mv $1_tlb.out* TLB_OUT
}


executePRFProcedure(){
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_prf.out1 1
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_prf.out2 2
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_prf.out3 4
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_prf.out4 8
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_prf.out5 16
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_prf.out6 32
	executePin 32 8 64 1024 8 128 64 4 4096 $1 $1_prf.out7 64
}

plotPRFGeneration(){
	$PLOT_PATH_PRF $1_prf.out1 $1_prf.out2 $1_prf.out3 $1_prf.out4 $1_prf.out5 $1_prf.out6 $1_prf.out7
	mv PRF.png $1_PRF.png
	mkdir PRF_OUT
	mv $1_prf.out* PRF_OUT
}



benchmark() {
	if [ "$1" == "L1" ]; then
	executeL1Procedure $2
	plotL1Generation $2
	fi
	
	if [ "$1" == "L2" ]; then
	executeL2Procedure $2
	plotL2Generation $2
	fi

	if [ "$1" == "TLB" ]; then
	executeTLBProcedure $2
	plotTLBGeneration $2
	fi

	if [ "$1" == "PRF" ]; then
	executePRFProcedure $2
	plotPRFGeneration $2
	fi
}

#for i in blackscholes bodytrack
#do
#	benchmark $i
#done

benchmark $1 $2

