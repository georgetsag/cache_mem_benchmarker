#!/usr/bin/env python

import sys
import numpy as np

## We need matplotlib:
## $ apt-get install python-matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

x_Axis = []
ipc_Axis = []
mpki_Axis = []

l1_size_base = 8.0
l1_assoc_base = 4.0

for outFile in sys.argv[2:]:
	fp = open(outFile)
	line = fp.readline()
	while line:
		tokens = line.split()
		if (line.startswith("Total Instructions: ")):
			total_instructions = long(tokens[2])
		elif (line.startswith("IPC:")):
			ipc = float(tokens[1])
		elif (line.startswith("  Data Tlb:")):
			sizeLine = fp.readline()
			l1_size = sizeLine.split()[1]
			bsizeLine = fp.readline()
			l1_bsize = bsizeLine.split()[2]
			assocLine = fp.readline()
			l1_assoc = assocLine.split()[1]
		elif (line.startswith("Tlb-Total-Misses")):
			l1_total_misses = long(tokens[1])
			l1_miss_rate = float(tokens[2].split('%')[0])
			mpki = l1_total_misses / (total_instructions / 1000.0)


		line = fp.readline()

	fp.close()

	if sys.argv[1] != "const_cycle":
		size_reduction = np.log2((float(l1_size) / l1_size_base))
		while (size_reduction > 0.0):
			ipc *= 0.909090909
			size_reduction -= 1.0
		size_gain = np.log2((l1_size_base / float(l1_size)))
		while (size_gain > 0.0):
			ipc *= 1.111111111
			size_gain -= 1.0
		assoc_reduction =  np.log2((float(l1_assoc) / l1_assoc_base))
		while (assoc_reduction > 0.0):
			ipc *= 0.952380952
			assoc_reduction -= 1.0
		assoc_gain = np.log2((l1_assoc_base / float(l1_assoc)))
		while (assoc_gain > 0.0):
			ipc *= 1.052631579
			assoc_gain -= 1.0

	l1ConfigStr = '{}K.{}.{}B'.format(l1_size,l1_assoc,l1_bsize)
	print l1ConfigStr
	x_Axis.append(l1ConfigStr)
	ipc_Axis.append(ipc)
	mpki_Axis.append(mpki)

print x_Axis
print ipc_Axis
print mpki_Axis

fig, ax1 = plt.subplots()
ax1.grid(True)
ax1.set_xlabel("TLBSize.Assoc.PageSize")

xAx = np.arange(len(x_Axis))
ax1.xaxis.set_ticks(np.arange(0, len(x_Axis), 1))
ax1.set_xticklabels(x_Axis, rotation=45)
ax1.set_xlim(-0.5, len(x_Axis) - 0.5)
ax1.set_ylim(min(ipc_Axis) - 0.05 * min(ipc_Axis), max(ipc_Axis) + 0.05 * max(ipc_Axis))
ax1.set_ylabel("$IPC$")
line1 = ax1.plot(ipc_Axis, label="ipc", color="red",marker='x')

ax2 = ax1.twinx()
ax2.xaxis.set_ticks(np.arange(0, len(x_Axis), 1))
ax2.set_xticklabels(x_Axis, rotation=45)
ax2.set_xlim(-0.5, len(x_Axis) - 0.5)
ax2.set_ylim(min(mpki_Axis) - 0.05 * min(mpki_Axis), max(mpki_Axis) + 0.05 * max(mpki_Axis))
ax2.set_ylabel("$MPKI$")
line2 = ax2.plot(mpki_Axis, label="TLB_mpki", color="green",marker='o')

lns = line1 + line2
labs = [l.get_label() for l in lns]

lgd = plt.legend(lns, labs)
lgd.draw_frame(False)

if sys.argv[1] == "const_cycle":
	plt.title("IPC vs MPKI for "+sys.argv[2].split('.')[0])
	plt.savefig("TLB.png",bbox_inches="tight")
else:
	plt.title("IPC vs MPKI for "+sys.argv[2].split('.')[0]+" with cycle change")
	plt.savefig(sys.argv[2].split('.')[0]+"_q2.png",bbox_inches="tight")
