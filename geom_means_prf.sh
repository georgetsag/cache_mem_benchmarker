#!/usr/bin/env python

import sys
import os
import numpy as np

## We need matplotlib:
## $ apt-get install python-matplotlib
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

x_Axis = []
ipc_Axis = []
mpki_Axis = []

for x in xrange(1,8):
	ipc_mean = 1
	mpki_mean = 1
	count = 0
	for file in os.listdir("PRF_OUT/"):
		if file.endswith(".out"+str(x)):
			print file
			fp = open("PRF_OUT/"+file)
			line = fp.readline()
			while line:
				tokens = line.split()
				if (line.startswith("Total Instructions: ")):
					total_instructions = long(tokens[2])
				elif(line.startswith("IPC:")):
					ipc = float(tokens[1])
				elif (line.startswith("L2-Total-Misses")):
					l1_total_misses = long(tokens[1])
					mpki = l1_total_misses / (total_instructions / 1000.0)
				line = fp.readline()
			fp.close()
			ipc_mean *= ipc
			mpki_mean *= mpki
			count += 1
	ipc_Axis.append((ipc_mean)**(1.0/count))
	mpki_Axis.append((mpki_mean)**(1.0/count))
	x_Axis.append(2**(x-1))
	
print x_Axis
print ipc_Axis
print mpki_Axis

fig, ax1 = plt.subplots()
ax1.grid(True)
ax1.set_xlabel("PrefetchedBlocksN")

xAx = np.arange(len(x_Axis))
ax1.xaxis.set_ticks(np.arange(0, len(x_Axis), 1))
ax1.set_xticklabels(x_Axis, rotation=45)
ax1.set_xlim(-0.5, len(x_Axis) - 0.5)
ax1.set_ylim(min(ipc_Axis) - 0.05 * min(ipc_Axis), max(ipc_Axis) + 0.05 * max(ipc_Axis))
ax1.set_ylabel("$IPC$")
line1 = ax1.plot(ipc_Axis, label="ipc", color="red",marker='x')

ax2 = ax1.twinx()
ax2.xaxis.set_ticks(np.arange(0, len(x_Axis), 1))
ax2.set_xticklabels(x_Axis, rotation=45)
ax2.set_xlim(-0.5, len(x_Axis) - 0.5)
ax2.set_ylim(min(mpki_Axis) - 0.05 * min(mpki_Axis), max(mpki_Axis) + 0.05 * max(mpki_Axis))
ax2.set_ylabel("$MPKI$")
line2 = ax2.plot(mpki_Axis, label="L2D_mpki", color="green",marker='o')

lns = line1 + line2
labs = [l.get_label() for l in lns]

lgd = plt.legend(lns, labs)
lgd.draw_frame(False)

plt.title("IPC vs MPKI, geometric means for PRF")
plt.savefig("PRF_means.png",bbox_inches="tight")
